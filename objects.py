#'''
#This is the user-defined object that describe the character and plate
#'''



import numpy as np
import cv2 as cv
import math
import os


class Character:

	def __init__(self, _rect):

		#Here we initialize the object
		#The rectshape is containing the left upper point and the width and height
		[a,b,c,d] = _rect;
		self.x = a
		self.y = b
		self.width = c
		self.height = d
		self.centerX = a + c/2
		self.centerY = b + d/2
		self.area = c*d
		self.diagonal = math.sqrt(c*c + d*d)
		self.offdiagonal = float(c)/float(d)


