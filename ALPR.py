#'''
#This code is served as the final project of ECE532
#The objective of this project is to successfully identify the license plate
#Dependency: Python-Numpy, OpenCV, Matplotlib, Scipy
#Author: Heng Liu
#Email: hengl@email.arizona.edu
#Date: 2016 Nov.16
#'''


import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import objects
from math import pi as PI
import math
import numpy
import scipy.io as sio

numpy.set_printoptions(threshold=numpy.nan)

image = cv.imread('test.png')
n = image.shape[0]
r = image.shape[1]

#Here we define some features for character
AREA = 80
WIDTH = 2
HEIGHT = 8
OFFDIAGmin = 0.25
OFFDIAGmax = 1

#Here we define some features for a license plate
#The change of characters must be smaller than this ratio
DIFFDIST = 5.0
DIFFAREA = 0.5 
DIFFWIDTH = 0.8
DIFFHEIGHT = 0.2
DIFFANGLE = 12.0

#We create a classifier
kNearest = cv.ml.KNearest_create()

training = sio.loadmat('training.mat')['training']


def main():
#'''
#This is the main call
#'''
	global image
	ImageThresholded = Threshold(image)
		

	##Here we employ the Contour extraction method as the plate extraction strategy
	##For better accuracy, we obtain the exact the contours
	##The findContours is returning the coontours list
	contours = cv.findContours(ImageThresholded, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)[1]
	
	##Here we filter the contours depending on the shape of contour
	character_list = filter(contours)[0]
	
	##Using the suspected characters, we group them to possible plate area
	possible_plate_list = group(character_list)
	
	##Using the possible groups of characters, we extract the area of plates from the original image
	##Corresponding image parts are stored in @plate_list_images
	plate_list_image = chop(possible_plate_list, image)

	##Here we use the possible_plate	
	Plates_number = recognize(plate_list_image)

	print Plates_number




def Threshold(image):
#'''
#This function read a BGR image and perform a set of analysis and return the thresholded image back
#@image: The original BGR image ready to process
#@ImageThresholded: The threshloded image using adaptive thresholding
#'''

	ImageGray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
	
	#Here we perform morphological oeration to enhance the contrast
	strEle = cv.getStructuringElement(cv.MORPH_RECT, (11,11))
	TopHat = cv.morphologyEx(ImageGray, cv.MORPH_TOPHAT, strEle)
	BlackHat = cv.morphologyEx(ImageGray, cv.MORPH_BLACKHAT, strEle)
	TopHat = cv.add(ImageGray, TopHat)
	final = cv.subtract(ImageGray, BlackHat)
	

	#Use gaussian filter to filter the noise, size can't be smaller than 5
	filtered = cv.GaussianBlur(final, (5,5), 0)

	
	#Here we do adaptive thresholding, weight can't be too small
	size = 19
	weight = 9
	thresholded = cv.adaptiveThreshold(filtered, 1.0, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV, size, weight)
	

	return thresholded


def filter(contours):
#'''
#This function is reading the list of the contours and filter the unpossible characters
#For the possible character contour, the width, height, ratio is in some range, we use this criteria
#To decide if a contour is a possible character, then instantite all contours as our user-defined objects
#@contours: The original list of contours 
#@LIST: The filtered list of characters
#'''
	global AREA, WIDTH, HEIGHT, OFFDIAGmin, OFFDIAGmax
	LIST = []
	Indices = []
	index = 0
	for item in contours:
		
		#This is finding the area infor of the contour
		rect = objects.Character(cv.boundingRect(item))
		if (rect.width > WIDTH and rect.height > HEIGHT and rect.area > AREA):
			if (rect.offdiagonal < OFFDIAGmax and rect.offdiagonal > OFFDIAGmin):
				LIST.append(rect)
				Indices.append(index)
		index = index + 1

	return LIST, Indices




def group(LIST):
#'''
#This function groups the characters into possible plate areas using some predefined properties of plate
#For possible plate, we assume the characters are restricted in a certain area
#@LIST: The original list of checked characters to group
#@candidatePlates: The list of possible plates
#'''
	possible_plates = []
	#Here we iterate each member of the contours to find its group 
	for i in LIST:
		possible_plates_item = innerGroup(i, LIST)
		possible_plates_item.append(i)
		if len(possible_plates_item) < 4: #no license plate will have this few characters 
			continue
		#We find a group, oh-yeah!!!
		possible_plates.append(possible_plates_item)
		rest = list(set(LIST) - set(possible_plates_item))
		rest_possible_plates = group(rest)
		for j in rest_possible_plates:
			possible_plates.append(j)
		break

	return possible_plates

def innerGroup(item, LIST):
#'''
#This is a child function called by parent function 'group'
#Here we use the distance, width, height, area to identify comarades of a character
#@item: certain contous
#@LIST: contour comarades candidates of the item contour 
#@comarades: a list of contour characters comarades of item 
#'''
	global DIFFDIST, DIFFAREA, DIFFWIDTH, DIFFHEIGHT, DIFFANGLE
	comarades = []
	for cont in LIST:
		if cont != item:
			h_dist = abs(cont.centerX - item.centerX)  #horizonatal distance
			v_dist = abs(cont.centerY - item.centerY)  #vectical distance
			distance = math.sqrt(h_dist * h_dist + v_dist * v_dist)
			DiffofArea = float(abs(cont.area - item.area))
			DiffofWidth = float(abs(cont.width - item.width))
			DiffofHeight = float(abs(cont.height - item.height))
			if h_dist != 0:
				DiffofAngle = math.atan(float(v_dist)/float(h_dist))*180/PI
			else:
				DiffofAngle = 1.57*180/PI

			if (distance/float(item.diagonal) < DIFFDIST and DiffofArea/float(item.area) < DIFFAREA):
				if (DiffofWidth/float(item.width) < DIFFWIDTH and DiffofHeight/float(item.height) < DIFFHEIGHT):
					if DiffofAngle < DIFFANGLE:
						comarades.append(cont)
	return comarades


def chop(possible_plate_list, image):
#'''
#This function is taking the geometrical information list as inputs, and output the candidate images of plates 
#@possible_plate_list: a list of groups of characters, each group of contours is possibly the characters of a plate
#@image: original BGR image
#@plate_list: the returned list of license plates images
#'''
	global n,r
	plate_list = [];
	for item in possible_plate_list:
		size = len(item)-1	
		
		#Here we use the characters to explore the possible shape of the plate and extract the plate portion image
		#After the sorting, the leftmost and rightmost candidates are the deciding candidates for the plate
		item.sort(key = lambda x: x.centerX)
		center = tuple([(item[size].centerX + item[0].centerX)/2, (item[size].centerY + item[0].centerY)/2])
		width = int((item[size].centerX - item[0].centerX + item[0].width)*1.3)
		h = 0
		for i in item:
			h = h + i.height
		heightofPlate = int(h/size * 1.5)

		#Here we are about to chop the part of the plate out of the image
		#we need to decide the orientation, size 
		height = item[size].centerY - item[0].centerY
		a = item[size].centerY - item[0].centerY
		b = item[size].centerX - item[0].centerX
		diagonal = math.sqrt(a*a + b*b)
		angle = math.asin(height/diagonal)*180/PI

		M = cv.getRotationMatrix2D(center, angle, 1.1) #enlarge by ten percent
		rotate = cv.warpAffine(image, M, (n,r)) #Rotate the image
		chopped = cv.getRectSubPix(rotate, (width, heightofPlate), tuple(center))

		plate_list.append(chopped)

	return plate_list



def recognize(Plate_images):
#'''
#In this function, we use the possible plate candidates to extract the information(characters)
#@possible_plate_list: The group of contours of corresponding plate image 
#@plate_list_image: The input argument is a list of candidate plates
#@Plates_number: The output argument is the potential license number of each corresponding contour group
#'''
	#Here we initiate the classifier
	KNN()
	Plates_number = []
	a = 1
	for i in Plate_images:
		##enlarge the thresholded image and Threshold again for the enlarged area 
		Image = Threshold(i)
		Image = cv.resize(Image, (0, 0), fx = 1.5, fy = 1.5)
		Image_copy = Image 
		
		
		##Find the contours within the plate image
		temp,contours,sdf = cv.findContours(Image_copy, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
		chars,rang = filter(contours)
		if a == 1:
			for q in rang:
				d = cv.drawContours(temp, contours,q, (255,255,0), 1)
				plt.imshow(Image)
				plt.show()
		
		
		chars.sort(key = lambda x: x.centerX)
		list = chars
		
		if len(list) > 0 and a == 1: 
			result = ''
			for j in list:
				temp_plate = Image[j.y:(j.y+j.height), j.x:(j.x+j.width)]
				#print np.array(temp_plate)
				chopp = cv.resize(temp_plate, (20,30)).reshape((1, 600))
				
				#plt.imshow(Image[j.y:(j.y+j.height), j.x:(j.x+j.width)])
				#plt.show()				
				chopp = np.float32(chopp)
				string = kNearest.findNearest(chopp, k = 3)[2][0][0]
				result = result + str(chr(int(string)))

			Plates_number.append(result)

		a = a + 1
	return Plates_number


def KNN():
#'''
#In this function we train a KNN classifier, no argument is needed
#'''

	global KNearest, training
	label = training[:, 600]
	data = training[:,0:600] 
	kNearest.train(data, cv.ml.ROW_SAMPLE, label) 

	return True


if __name__ == '__main__':
	main()

